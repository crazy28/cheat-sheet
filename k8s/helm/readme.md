### helm3 version
```
helm version
```
### install helm chart
```
helm install <chart-name> <chart-location>
helm install grafana .
helm install grafana . --namespace <namespace>

helm install <release-name> <chart-name>
```
### delete a release
```
helm del <release-name>
helm del <release-name> --namespace <namespace>

[note: --purge is not required as it is handled by default]
```
### add helm repo
```
helm repo add stable https://kubernetes-charts.storage.googleapis.com
helm repo ls
helm fetch stable/prometheus-operator
helm install <release-name> -n <namespace> stable/prometheus-operator
```

### list the repo and search the package based on repo ###
```
helm repo ls   # To list helm repo's locally which added
helm search repo nginx.    # search nginx in all repo's that you have (i.e locally)
helm search repo bitnami   # list all the packages in bitnami
helm search repo bitnami | grep nginx.   # grep nginx to filter out from bitnami repo
```

### custom values from file ###
![](/uploads/f392a22ecf595a7378ad9aacdac85aae/Screenshot_2022-04-11_at_1.06.20_PM.png)
